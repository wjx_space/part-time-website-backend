package com.wjx.ptjob.gateway.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @since:2024/2/20 16:39
 * @author:wujiaxiong
 * @version:1.0.0
 **/
@RestController
public class Demo {

    @Value("${wjx}")
    private String test;

    @RequestMapping("test001")
    public void test001(){
        System.out.println(test);
    }

}
