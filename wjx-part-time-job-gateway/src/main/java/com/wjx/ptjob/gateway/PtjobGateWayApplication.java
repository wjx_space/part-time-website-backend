package com.wjx.ptjob.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

/**
 * @description:兼职工作网关服务启动类
 * @since:2024/2/20 15:11
 * @author:wujiaxiong
 * @version:1.0.0
 **/
@SpringBootApplication
@EnableWebFlux
public class PtjobGateWayApplication {
    public static void main(String[] args) {
        SpringApplication.run(PtjobGateWayApplication.class,args);
    }
}
