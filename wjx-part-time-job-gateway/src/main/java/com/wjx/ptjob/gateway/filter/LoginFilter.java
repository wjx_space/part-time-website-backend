package com.wjx.ptjob.gateway.filter;

import cn.hutool.core.collection.CollUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;


/**
 * @description:
 * @since:2023/12/8 18:05
 * @author:wujiaxiong
 * @version:1.0.0
 **/
@Component
public class LoginFilter implements GlobalFilter, Ordered {

    AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    private static String TOKEN_PRE="token:";

//    @Autowired
//    public void setApplicationContext(ApplicationContext applicationContext) {
//        applicationContext = applicationContext;
//    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        System.out.println(123);
        //认证鉴权
        HttpHeaders headers = request.getHeaders();
        List<String> list = headers.get("token");


        //放开登录接口，有些草率
        String path = request.getPath().toString();
        if (!"/auth-route/auth/login".equals(path)){

            if(CollUtil.isEmpty(list)){
                throw new RuntimeException("请登录");
            }
            String token = list.get(0);
            //查询reids是否有这个token

            String s = redisTemplate.opsForValue().get(TOKEN_PRE+token);
            if(null==s){
                throw new RuntimeException("请登录");
            }
        }


//        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
//        applicationContext.refresh();
//        RequestMappingHandlerMapping bean = applicationContext.getBean(RequestMappingHandlerMapping.class);


        return chain.filter(exchange);

        //示例代码
//        ServerHttpRequest request = exchange.getRequest();
//        ServerHttpResponse response = exchange.getResponse();
//        //过滤的业务代码
//        //1.获取请求路径---
//        String path = request.getPath().toString();
//        //判断该路径是否放行路径
//        if(filterUrl.getWhitePaths().contains(path)){
//            return chain.filter(exchange);
//        }
//
//        //获取请求头
//        String token = request.getHeaders().getFirst("token");
//        if(StringUtils.hasText(token)&&"admin".equals(token)){ //查看redis中是否存在该token
//            return chain.filter(exchange);//放行
//        }
//        //json数据
//        Map<String, Object> map = new HashMap<>();
//        map.put("msg", "未登录");
//        map.put("code", 403);
//
//        //3.3作JSON转换
//        byte[] bytes = JSON.toJSONString(map).getBytes(StandardCharsets.UTF_8);
//        //3.4调用bufferFactory方法,生成DataBuffer对象
//        DataBuffer buffer = response.bufferFactory().wrap(bytes);
//
//        //4.调用Mono中的just方法,返回要写给前端的JSON数据
//        return response.writeWith(Mono.just(buffer));
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
